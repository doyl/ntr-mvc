﻿using System;
using System.Collections.Generic;

namespace Z01.Utility.DataManager
{
    public interface IDataManager<T>
    {
        void Add(T item);
        void Remove(T item);
        void Update(T item);
        T Find(Func<T, bool> checkingFunction);
        IEnumerable<T> FindAll();
        IEnumerable<T> FindAll(Func<T, bool> checkingFunction);
    }
}