﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Z01.Database;
using Z01.Database.Model;
using Note = Z01.Models.Note;
using DbNote = Z01.Database.Model.Note;

namespace Z01.Utility.DataManager
{
    public class NotesDatabaseDataManger : IDataManager<Note>
    {
        public void Add(Note item)
        {
            NotepadDbContext ctx = new NotepadDbContext();
            DbNote note = DatabaseModelConverter.ToDbNote(item, ctx);
            ctx.Notes.Add(note);
            ctx.SaveChanges();
        }

        public void Remove(Note item)
        {
            NotepadDbContext ctx = new NotepadDbContext();
            //DbNote note = ctx.Notes.First(t => t.Title == item.Title);
            DbNote note = ctx.Notes.Include(i => i.NotesCategories)
                .ThenInclude(i => i.Category)
                .FirstOrDefault(i => i.Title == item.Title);
            
            if (note == null) 
            {
                return;
            }
            foreach (NoteCategory category in note.NotesCategories) 
            {
                if (ctx.NotesCategories.FirstOrDefault(i => i.CategoryID == category.Category.CategoryID && i.Note.Title != item.Title) == null) 
                {
                    ctx.Categories.Remove(category.Category);
                }
            }
            
            ctx.Notes.Remove(note);
            ctx.SaveChanges();
        }

        public void Update(Note item)
        {
            var ctx = new NotepadDbContext();
            var oldNote = ctx.Notes.Include(i => i.NotesCategories)
                .ThenInclude(i => i.Category)
                .FirstOrDefault(i => i.NoteID == item.Id); //TODO: Should search by id

            if (oldNote == null)
            {
                throw new NotepadUpdateException("Note deleted by another user or process! Please, cancel your edition!");
            }

            ctx.Entry(oldNote).Property("Timestamp").OriginalValue = item.Timestamp;

            oldNote.Title = item.Title;
            oldNote.NoteDate = item.Date;
            oldNote.Description = item.Content;

            DeleteRedundantCategories(oldNote.NotesCategories, item, ctx);
            
            oldNote.FillCategories(ctx, item.Categories);
            try
            {
                ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                var exceptionEntry = ex.Entries.Single();
                var databaseEntry = exceptionEntry.GetDatabaseValues();

                if (databaseEntry == null)
                {
                    throw new NotepadUpdateException("Note deleted by another user or process! Please, cancel your edition!");
                }
                else
                {
                    throw new NotepadUpdateException("Note edited by another user or process! Please, cancel your edition!");
                }
            }
        }

        public Note Find(Func<Note, bool> checkingFunction)
        {
            IEnumerable<Note> parsedNotes = FindAll();
            foreach (Note n in parsedNotes)
            {
                if (checkingFunction(n))
                    return n;
            }

            return null;
        }

        public IEnumerable<Note> FindAll()
        {
            NotepadDbContext ctx = new NotepadDbContext();
            var notesList = ctx.Notes.Include(i => i.NotesCategories)
                .ThenInclude(i => i.Category)
                .AsNoTracking().ToList();
            IEnumerable<Note> notes = notesList.Select(DatabaseModelConverter.ToNote);
            return notes;
        }

        public IEnumerable<Note> FindAll(Func<Note, bool> checkingFunction)
        {
            throw new NotImplementedException();
        }

        public void DeleteRedundantCategories(ICollection<NoteCategory> notesCategories, Note item, NotepadDbContext ctx)
        {
            foreach (var category in notesCategories)
            {
                if (!item.Categories.Contains(category.Category.Title))
                {
                    if (ctx.NotesCategories.FirstOrDefault(n => n.CategoryID == category.Category.CategoryID && n.NoteID != item.Id) == null)
                    {
                        ctx.Categories.Remove(category.Category);
                    }
                }
            }
        }
    }
}

public class NotepadUpdateException : Exception
{
    public NotepadUpdateException()
    {
    }

    public NotepadUpdateException(string message)
        : base(message)
    {
    }

    public NotepadUpdateException(string message, Exception inner)
        : base(message, inner)
    {
    }
}