﻿using System.Collections.Generic;
using Z01.Database.Model;
using Note = Z01.Models.Note;
using DbNote = Z01.Database.Model.Note;
using System.Linq;
using Z01.Database;

namespace Z01.Utility
{
    public static class DatabaseModelConverter
    {
        public static Note ToNote(DbNote note)
        {
            List<string> categories = GetCategories(note);
            Note newNote = new Note(note.Title, note.Description, note.NoteDate, categories, false, note.NoteID, note.Timestamp);
            return newNote;
        }

        private static List<string> GetCategories(DbNote note)
        {
            return note.NotesCategories.Select(t => t.Category.Title).ToList();
        }

        public static DbNote ToDbNote(Note note, NotepadDbContext context)
        {
            DbNote newNote = new DbNote {Description = note.Content, Title = note.Title, NoteDate = note.Date};
            newNote.FillCategories(context, note.Categories);
            return newNote;
        }

        private static void AddCategory(Category category, NotepadDbContext context)
        {
            context.Categories.Add(category);
        }
        public static void FillCategories(this DbNote note, NotepadDbContext context, List<string> categories) {
            List<NoteCategory> noteCategories = new List<NoteCategory>();
            categories.ForEach(c =>
            {
                Category category = context.Categories.FirstOrDefault(t => c==t.Title);
                if (category == null)
                {
                    category = new Category {Title = c};
                    AddCategory(category, context);
                }

                noteCategories.Add(new NoteCategory{ Note = note, Category = category});
            });
            note.NotesCategories = noteCategories;
        }
    }
}