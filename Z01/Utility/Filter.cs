﻿using System;
using System.Collections.Generic;
using System.Linq;
using Z01.Models;

namespace Z01.Utility
{
    [System.Serializable]
    public class Filter
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Category { get; set; }

        public Filter()
        {
            FromDate = DateTime.MinValue;
            ToDate = DateTime.MaxValue;
            Category = string.Empty;
        }
        public Filter(DateTime fromDate, DateTime toDate, string category)
        {
            FromDate = fromDate;
            ToDate = toDate;
            Category = category == "{All}" ? string.Empty : category;
        }

        public IEnumerable<Note> Apply(IEnumerable<Note> allNotes)
        {
            return allNotes.Where(t => t.Date < ToDate && t.Date > FromDate && (Category == "{All}" || t.Categories.Contains(Category)));
        }
    }
}