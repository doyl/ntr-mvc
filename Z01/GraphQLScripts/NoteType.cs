﻿using GraphQL.Types;
using Z01.Models;

namespace Z01.GraphQLScripts
{
    public class NoteType : ObjectGraphType<Note>
    {
        public NoteType()
        {
            Field(x => x.Id);
            Field(x => x.Title);
            Field(x => x.Date);
            Field(x => x.IsMarkdown);
            Field(x => x.Content);
            Field(x => x.Categories);
        }
    }
}