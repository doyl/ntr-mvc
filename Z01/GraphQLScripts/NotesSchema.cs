﻿using GraphQL;

namespace Z01.GraphQLScripts
{
    public class NotesSchema : GraphQL.Types.Schema
    {
        public NotesSchema(IDependencyResolver dependencyResolver) : base(dependencyResolver)
        {
            Query = dependencyResolver.Resolve<NoteQuery>();
        }
    }
}