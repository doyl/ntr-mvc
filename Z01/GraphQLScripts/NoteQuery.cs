﻿using GraphQL.Types;
using Z01.Models;
using Z01.Utility.DataManager;

namespace Z01.GraphQLScripts
{
    public class NoteQuery : ObjectGraphType
    {
        IDataManager<Note> dataSource = new NotesDatabaseDataManger();

        public NoteQuery()
        {
            Field<ListGraphType<NoteType>>("notes", resolve: context => dataSource.FindAll());
        }
    }
}