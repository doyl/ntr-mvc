﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Z01.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "dygas");

            migrationBuilder.CreateTable(
                name: "Categories",
                schema: "dygas",
                columns: table => new
                {
                    CategoryID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(maxLength: 64, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.CategoryID);
                });

            migrationBuilder.CreateTable(
                name: "Notes",
                schema: "dygas",
                columns: table => new
                {
                    NoteID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(maxLength: 64, nullable: false),
                    NoteDate = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notes", x => x.NoteID);
                });

            migrationBuilder.CreateTable(
                name: "NotesCategories",
                schema: "dygas",
                columns: table => new
                {
                    NoteID = table.Column<int>(nullable: false),
                    CategoryID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotesCategories", x => new { x.NoteID, x.CategoryID });
                    table.ForeignKey(
                        name: "FK_NotesCategories_Categories_CategoryID",
                        column: x => x.CategoryID,
                        principalSchema: "dygas",
                        principalTable: "Categories",
                        principalColumn: "CategoryID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_NotesCategories_Notes_NoteID",
                        column: x => x.NoteID,
                        principalSchema: "dygas",
                        principalTable: "Notes",
                        principalColumn: "NoteID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_NotesCategories_CategoryID",
                schema: "dygas",
                table: "NotesCategories",
                column: "CategoryID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NotesCategories",
                schema: "dygas");

            migrationBuilder.DropTable(
                name: "Categories",
                schema: "dygas");

            migrationBuilder.DropTable(
                name: "Notes",
                schema: "dygas");
        }
    }
}
