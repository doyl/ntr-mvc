﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Z01.Migrations
{
    public partial class AddedTimestamp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "Timestamp",
                schema: "dygas",
                table: "Notes",
                rowVersion: true,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Timestamp",
                schema: "dygas",
                table: "Notes");
        }
    }
}
