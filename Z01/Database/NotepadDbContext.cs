﻿using System;
using System.Diagnostics;
using Microsoft.EntityFrameworkCore;
using Z01.Database.Model;

namespace Z01.Database
{
    public class NotepadDbContext : DbContext
    {
        public DbSet<Note> Notes { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<NoteCategory> NotesCategories { get; set; }
        
        public NotepadDbContext(DbContextOptions<NotepadDbContext> options) : base(options){}

        public NotepadDbContext() : base()
        {
            Console.Write(Database.GetDbConnection().ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("dygas");
            modelBuilder.Entity<Note>().Property(n => n.Title).IsRequired().HasMaxLength(64);
            modelBuilder.Entity<Note>().Property(n => n.NoteDate).IsRequired();
            modelBuilder.Entity<Note>().Property(n => n.Description);
            modelBuilder.Entity<Note>().HasKey(n => n.NoteID);
            modelBuilder.Entity<Note>().Property(n => n.Timestamp).IsConcurrencyToken(); 

            modelBuilder.Entity<Category>().Property(c => c.Title).IsRequired().HasMaxLength(64);
            modelBuilder.Entity<Category>().HasKey(c => c.CategoryID);

            modelBuilder.Entity<NoteCategory>().HasKey(nc => new {nc.NoteID, nc.CategoryID});

            modelBuilder.Entity<NoteCategory>()
                .HasOne<Note>(nc => nc.Note)
                .WithMany(n => n.NotesCategories)
                .HasForeignKey(nc => nc.NoteID);

            modelBuilder.Entity<NoteCategory>()
                .HasOne<Category>(nc => nc.Category)
                .WithMany(c => c.NotesCategories)
                .HasForeignKey(nc => nc.CategoryID);
            
            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer("Server=localhost,8200;Database=NTR2019Z;User Id=dygas; Password=283730;");
            optionsBuilder.UseSqlServer (
                @"Server=base.ii.pw.edu.pl,1433;Database=NTR2019Z;User Id=dygas;Password=283730;");
        }
    }
}