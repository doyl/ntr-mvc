﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Z01.Models;

namespace Z01.Database.Model
{
    public class Note
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NoteID { get; set; }
        public string Title { get; set; }
        public DateTime NoteDate { get; set; }
        public string Description { get; set; }
        
        [Timestamp]
        public byte[] Timestamp {get; set;}
        public ICollection<NoteCategory> NotesCategories { get; set; }
    }
}