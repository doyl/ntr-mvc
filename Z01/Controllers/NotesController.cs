﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphQL;
using GraphQL.Types;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Z01.GraphQLScripts;
using Z01.Models;
using Z01.Models.ViewModels;
using Z01.Utility;
using Z01.Utility.DataManager;

namespace Z01.Controllers
{
    [Route("[controller]")]
    public class NotesController : Controller
    {
        private int notesPerPage = 4; 
        //private readonly IDataManager<Note> notes = new NotesFilesManager();
        private readonly IDataManager<Note> notes = new NotesDatabaseDataManger();
        private ISchema schema;
        private IDocumentExecuter documentExecuter;
        public NotesController(ISchema schema, IDocumentExecuter documentExecuter)
        {
            this.schema = schema;
            this.documentExecuter = documentExecuter;
        }

        [HttpPost]
        public async Task<IActionResult> Notes([FromBody] GraphQLQuery query)
        {
            if(query==null) 
                throw new ArgumentNullException("No query sent!");

            var inputs = query.Variables?.ToInputs();
            var executionOptions = new ExecutionOptions
            {
                Schema = schema,
                Query = query.Query,
                Inputs = inputs
            };

            var result = await documentExecuter.ExecuteAsync(executionOptions);

            if (result.Errors?.Count>0)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }
        
        public IActionResult Index()
        {
            HttpContext.Session.Remove("OldTitle");
            HttpContext.Session.Remove("OldInfo");
            //IDataManager<Note> notesManager = new NotesFilesManager();
            IEnumerable<Note> allNotes = notes.FindAll().ToList();
            IEnumerable<Note> filteredNotes;
            IEnumerable<string> categories = GetAllCategories(allNotes);
            if (HttpContext.Session.TryGet<Filter>("Filter", out Filter filter))
            {
                filteredNotes = filter.Apply(allNotes);
            }
            else
            {
                filteredNotes = allNotes;
            }

            filteredNotes = ApplyPagination(filteredNotes, out int currentPage, out int pagesCount);
            return View(new NotesIndexViewModel(filteredNotes, filter, categories, currentPage, pagesCount));
        }

        private IEnumerable<string> GetAllCategories(IEnumerable<Note> allNotes)
        {
            List<string> categories = new List<string>();
            foreach (Note n in allNotes)
            {
                foreach (string category in n.Categories)
                {
                    if (!categories.Contains(category))
                    {
                        categories.Add(category);
                    }
                }
            }
            categories.Sort((x, y) => x.CompareTo(y));
            return categories;
        }

        private IEnumerable<Note> ApplyPagination(IEnumerable<Note> notes, out int currentPage, out int pagesCount)
        {
            List<Note> notesList = notes.ToList();
            int count = notesList.Count;
            pagesCount = (int)Math.Ceiling((float)count / notesPerPage);
            if (!HttpContext.Session.TryGet("page", out currentPage))
            {
                currentPage = 0;
            }

            currentPage = Math.Min(currentPage, pagesCount-1);
            if (pagesCount > 0 && currentPage == -1)
            {
                currentPage = 0;
            }
            HttpContext.Session.Set("page", currentPage);
            int notesOnPage = Math.Min(count - currentPage * notesPerPage, notesPerPage);
            return notesList.Skip(currentPage * notesPerPage).Take(notesOnPage);
        }

        public IActionResult GoToPage(int page)
        {
            HttpContext.Session.Set("page", page);
            return RedirectToAction(nameof(Index));
        }
        
        public IActionResult New()
        {
            HttpContext.Session.Remove("OldTitle");
            return View(new Note());
        }

        public IActionResult Filter(Filter filter)
        {
            HttpContext.Session.Set<Filter>("Filter", filter);
            return RedirectToAction(nameof(Index));
        }

        public IActionResult ClearFilter()
        {
            HttpContext.Session.Remove("Filter");
            return RedirectToAction(nameof(Index));
        }
        public IActionResult Edit(string title)
        {
            Note note = notes.Find(t => t.Title == title);
            NoteIdentificationInfoData idInfo = new NoteIdentificationInfoData
                {Id = note.Id, Title = note.Title, Timestamp = note.Timestamp};
            HttpContext.Session.Set("OldInfo", idInfo);
            HttpContext.Session.Set("OldTitle", Encoding.ASCII.GetBytes(title));
            return View(note);
        }
        
        [HttpPost]
        public IActionResult Edit(string button, Note note, string categoryName)
        {
            if (button == "Add")
            {
                if (!string.IsNullOrEmpty(categoryName))
                {
                    if (!note.Categories.Contains(categoryName))
                    {
                        note.Categories.Add(categoryName);
                    }
                    else 
                        ModelState.AddModelError("Categories", "Category \"" + categoryName + "\" already exists!");
                }
                else
                {
                    ModelState.AddModelError("Categories", "Can't add empty category!'");
                }

                return View(note);
            }

            if (button == "Remove")
            {
                if (!string.IsNullOrEmpty(categoryName))
                {
                    if (note.Categories.Contains(categoryName))
                        note.Categories.Remove(categoryName);
                }
                return View(note);
            }
            
            if (ModelState.IsValid)
            {
                bool editing = false;
                //if (HttpContext.Session.TryGetValue("OldTitle", out oldTitleBytes))
                if(HttpContext.Session.TryGet("OldInfo", out NoteIdentificationInfoData oldInfo))
                {
                    string oldTitle = oldInfo.Title;//Encoding.ASCII.GetString(oldTitleBytes);
                    if (oldTitle != note.Title)
                    {
                        Note corruptingNote = notes.Find(x => x.Title == note.Title);
                        if (corruptingNote != null)
                        {
                            ModelState.AddModelError("Title", "Note with the same title already exists!");
                            return View(note);
                        }
                    }
                    else
                    {
                        editing = true;
                    }
                    //notes.Remove(notes.Find(t => t.Title == oldTitle));
                    //HttpContext.Session.Remove("OldInfo");
                }
                if(!editing)
                {
                Note corrupting = notes.Find(x => x.Title == note.Title);
                if (corrupting != null)
                {
                    ModelState.AddModelError("Title", "Title \"" + note.Title + "\" is taken!");
                    return View(note);
                }
                
                if (note.Title == "")
                {
                    ModelState.AddModelError("Title", "Title can't be empty!");
                    return View(note);
                }
                
                if (note.Title.ContainsAny('.', '/', '\\', '?', '%', '*', ':', '|', '"', '<', '>'))
                {
                    ModelState.AddModelError("Title", "Title contains inappropriate character!");
                    return View(note);
                }
                }

                if (HttpContext.Session.TryGet("OldInfo", out NoteIdentificationInfoData oldInfo2))
                {
                    note.Id = oldInfo.Id;
                    note.Timestamp = oldInfo.Timestamp; //Am i sure?
                    try
                    {
                        notes.Update(note);
                    }
                    catch (NotepadUpdateException ex)
                    {
                        ModelState.AddModelError("Title", ex.Message);
                        return View(note);
                    }
                    catch (DbUpdateException ex)
                    {
                        ModelState.Clear();
                        ModelState.AddModelError("Title", ex.Message);
                        return View(note);
                    }
                }
                else
                {
                    notes.Add(note);
                }

                return RedirectToAction(nameof(Index));
            }
            return View(note);
        }
        
        public IActionResult Delete(string title)
        {
            notes.Remove(notes.Find(t => t.Title == title));
            return RedirectToAction(nameof(Index));
        }
    }
}