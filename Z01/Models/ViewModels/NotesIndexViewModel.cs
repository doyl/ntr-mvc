﻿using System.Collections.Generic;
using Z01.Utility;

namespace Z01.Models.ViewModels
{
    public class NotesIndexViewModel
    {
        public IEnumerable<Note> Notes { get; set; }
        public Filter Filter { get; set; }
        public IEnumerable<string> Categories { get; set; }

        public int CurrentPage { get; set; }
        public int PagesCount { get; set; }

        public NotesIndexViewModel(IEnumerable<Note> notes, Filter filter, IEnumerable<string> categories, int currentPage, int pagesCount)
        {
            Notes = notes;
            Filter = filter;
            Categories = categories;
            CurrentPage = currentPage;
            PagesCount = pagesCount;
        }
    }
}