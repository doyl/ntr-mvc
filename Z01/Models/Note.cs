﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Z01.Models
{
    public class Note
    {
        [Required]
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
        public List<string> Categories { get; set; }
        public bool IsMarkdown { get; set; }
        
        public int Id { get; set; }
        
        public byte[] Timestamp {get; set;}

        public Note()
        {
            Title = "New Note";
            Content = "";
            Date = DateTime.Today;
            Categories = new List<string>();
            IsMarkdown = false;
            Id = 0;
            Timestamp = null;
        }
        
        public Note(string title, string content, DateTime date, IEnumerable<string> categories, bool isMarkdown, int id=0, byte[] timestamp=null)
        {
            Title = title;
            Content = content;
            Date = date;
            Categories = categories.ToList();
            IsMarkdown = isMarkdown;
            Id = id;
            Timestamp = timestamp;
        }
    }
}