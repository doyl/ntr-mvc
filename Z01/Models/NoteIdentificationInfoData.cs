﻿namespace Z01.Models
{
    public class NoteIdentificationInfoData
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public byte[] Timestamp { get; set; }
    }
}